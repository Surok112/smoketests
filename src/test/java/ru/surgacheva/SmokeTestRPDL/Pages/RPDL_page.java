package ru.surgacheva.SmokeTestRPDL.Pages;

import ru.surgacheva.SmokeTestRPDL.RDPL_methods;
import org.openqa.selenium.By;
import static ru.surgacheva.SmokeTestRPDL.DriverSetUp.driver;
import ru.surgacheva.SmokeTestRPDL.pdl.sql_query;

public class RPDL_page extends RDPL_methods{
    
    protected String indentificationName = ""; 

   //загрузка файла
    public void LoadFile(String path_file) throws InterruptedException{        
        Click(button_select, path_file);
        indentification.clear();
        indentificationName = "Список РПДЛ";
        indentification.sendKeys(indentificationName);  
        button_OK.click();
        WaitProcessInvisibility(buttonOK,1000);
        button_load.click();
    }
    
    //процесс передачи файла на обработку
    public void SaveFile() throws InterruptedException {        
        button_save.click();
        WaitButton(By.xpath(dialogButYes), 1000);
        dialogButtonYes.click();
        
        Thread.sleep(1000);
        WaitButton(By.xpath(buttonClose), 1000);  
        button_close.click();
    }


    //настройка правил
    public void SettingRules() throws InterruptedException {
        WaitProcess(oneRules, 1000);  
        int rules_Pdl = 4;
        Object [] rules_ = Random_mass (rules_Pdl, 1, rules_Pdl);
        //Object [] o = {};
        int [] rules = new int[rules_.length];
        for(int i = 0; i < rules_.length; i++) {
            rules[i] = (int) rules_[i];
            Click(By.xpath(".//*[@id='acceptGrid']/table/tbody/tr[" + rules[i] + "]/td[1]/div/input"));
        }  
        
        int rules_NotPdl = 2;
        rules_ = Random_mass (rules_NotPdl, 1, rules_NotPdl);
       // o = {};
        rules = new int[rules_.length];
        for(int i = 0; i < rules_.length; i++) {
            rules[i] = (int) rules_[i];
            Click(By.xpath("  .//*[@id='rejectGrid']/table/tbody/tr[" + rules[i] + "]/td[1]/div/input"));
        }
        buttonSaveRules.click();
        Thread.sleep(1000);
        WaitButton(By.xpath(buttonClose2), 1000);
        button_close2.click(); 
     }
    
    //Обработка совпадений
    public void ProcessingMatch() {
        WaitButton(By.xpath(buttonClose3), 1000);
        button_close3.click();
    }
    
   //Изменение выборов по совпадениям
    public void changeSelection() throws InterruptedException {
       WaitProcessInvisibility(buttonClose3,1000);
       changePage.click();
    }
    
    //Изменение галки у фигурната
    public void changeVerdict(int count) throws InterruptedException, Exception {
        int line = Random_value (1, count);
        doubleClick(By.xpath(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr["+ line +"]/td[1]"));
        Click(button_window);
        WaitProcess(editIdCheck, 1000);
        String figId = driver.findElement(By.xpath(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr["+ line +"]/td[1]")).getText();
       
     //   Thread.sleep(3000);
        //Ищем значение id которого будем редактировать
        String value  = driver.findElement(By.xpath(editIdCheck)).getText();
        
        String sqlquery = sql_query.SELECT_ISPDL_PDLVERDICT + value + " AND PDL_ID=" + figId;
        //запрос к базе, смотрим 0 или 1
        String result1 = Select(sqlquery);
        String result_1 = Str (result1);
    
        scrollDown(editCheck);
        Click(By.xpath(editCheck));
        Thread.sleep(2000);
        Click(SaveClose);
        //ожидание закрытия окна с вердиктом
        WaitProcessInvisibility(windowVerdict, 1000);
     //   Thread.sleep(1000);
        String result2 = Select(sqlquery);
        String result_2= Str (result2);
        AssertChangeVerdict(value, Integer.parseInt(result_1), Integer.parseInt(result_2));
    }
    
    //Добавление комментария
    public String [] AddComment(int count) throws InterruptedException, Exception {
        String [] val = new String [5];
        int line = Random_value (1, count);
        doubleClick(By.xpath(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr["+ line +"]/td[1]"));
        val[0] = String.valueOf(line);
       // Thread.sleep(1000);
        Click(button_window);
        WaitProcess(editIdCheck, 1000);
        String figId = driver.findElement(By.xpath(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr["+ line +"]/td[1]")).getText();
        val[1] = figId;
        //WaitProcess(editIdCheck, 1000);
     //   Thread.sleep(1000);
        //Ищем значение id которого будем редактировать
        String value  = driver.findElement(By.xpath(editIdCheck)).getText();
        val[2] = value;
        scrollDown(editComment);
        Click(editComment);
        comment.clear();
        String addComment = "ФигурантФигурантФигурант";
        val[3] = addComment;
        comment.sendKeys(addComment);  
        Click(saveComment);
        //ожидание закрытия окна с комментарием
        WaitProcessInvisibility(windowComment, 1000);
        Click(SaveClose);
        //ожидание зарктия окна с вердиктом
        WaitProcessInvisibility(windowVerdict, 1000);
     //   Thread.sleep(1000);
        String sqlquery = sql_query.SELECT_COMMENT_PDLVERDICT + value + " AND PDL_ID=" + figId;
        String result = Str(Select(sqlquery));
        val[4] = result;
        return val;
    }   
    
    public void AddComment_(int count) throws Exception{
        String [] val = AddComment(count); 
        AssertAddComment(val[1], val[3], val[4]); 
    }
     
    //Отображение комментария
    public void ViewComment(int count) throws InterruptedException, Exception {
        String [] val = AddComment(count); 
        //ожидание зарктия окна с вердиктом
        Thread.sleep(1000);
        doubleClick(By.xpath(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr["+ val[0] +"]/td[1]"));    
        Click(button_window); 
        //String value  = driver.findElement(By.xpath(textComment)).getText();
        scrollDown(textComment);
        String value  = driver.findElement(By.xpath(textComment)).getText();
        AssertViewComment(val[1], value, val[3]); 
    }   
}


        
       // driver.switchTo().();
        //проверка - открылось диалоговое окно
//        Alert alert = driver.switchTo().alert();
      //  String ss1 = alert.getText();

       //  WebDriverWait wait = new WebDriverWait(driver, 15, 100);
        // wait.until(ExpectedConditions.alertIsPresent());
     //     driver.switchTo().alert();
     //   Assert.assertTrue("Сбой! Диалоговое окно не открыто!", isAlertPresent() == true ); 
       //  isAlertPresent().Dismiss();