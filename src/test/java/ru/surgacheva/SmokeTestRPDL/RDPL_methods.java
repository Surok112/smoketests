package ru.surgacheva.SmokeTestRPDL;

import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import static ru.surgacheva.SmokeTestRPDL.DriverSetUp.driver;
import ru.surgacheva.SmokeTestRPDL.pdl.sql_query;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.jdom.input.SAXBuilder;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.openqa.selenium.JavascriptExecutor;
import java.io.*;
import java.util.*;

public class RDPL_methods extends Asserts {
     
    //количество фигурантов переданных на обработку
    public int Count(By locator) {
        int count = driver.findElements(locator).size(); 
        return count;
    }
    
    public int DoNotSend() throws InterruptedException {
        Thread.sleep(1000);
        int count = Count(By.xpath(count_row_table)); 
        //половина списка не будет отправлено на обработку
        int n = count/2;
        Object [] donotsent_ = Random_mass (n, 1, count);
        //Object [] o = {};
        int [] donotsent = new int[donotsent_.length];
        for(int i = 0; i < donotsent_.length; i++) {
            donotsent[i] = (int) donotsent_[i];
            Click(By.xpath(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr["+ donotsent[i]+"]/td[9]/div/div/input"));
        }  
        return donotsent.length;
    }
    
    //id фигурантов которые не отправлены на обработку
     public String [] CheckDoNotSend(String object, int n, int count) throws InterruptedException {
        String [] check = new  String [n];
        boolean isChecked;
        int k = 0;
         for (int i = 0; i < count; i++ ){
           //  Thread.sleep(1000);
            WebElement e = driver.findElement(By.xpath(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[" + (i+1) + "]/td[9]/div/div/input"));   
            isChecked = e.isSelected();
 
            if(isChecked){
                String text = AddValue (object, i+1, 1);   
                check [k] = text;
                k++;
            }
        }
        return check;
    }
     
    //провека что FOR_SEARCH = 0
    public void CheckFOR_SEARCH(String [] table, String strQuery) throws Exception{     
        for (int i = 0; i < table.length; i++){
           String val = Select(strQuery+table[i]);
           String value =  Str(val);
           AssertCheckFOR_SEARCH(table[i],Integer.parseInt(value));    
        }
    }
    
    //список результирующей таблицы
    public  Map<String, String> KeyValueProcessTable(String object, int count, int column1, int column2) {
        Map<String, String> hashmap = new HashMap<String, String>();
        for (int i = 0; i < count; i++){
            hashmap.put(AddValue(object, i+1, column1), AddValue(object, i+1, column2));
        }
        return hashmap;
    }
    
     //список результирующей таблицы
    public  Map<String, String> KeyValueProcessTable(String object, int count, int column) {
        Map<String, String> hashmap = new HashMap<String, String>();
        for (int i = 0; i < count; i++){
            hashmap.put(Integer.toString(i+1), AddValue(object, i+1, column));
        }
        return hashmap;
    }
         
    //Click-нажатие кнопок
    public void Click(WebElement button){
        button.click(); 
    }
    
    public void Click(By locator){
        driver.findElement(locator).click();
    }
    
    public  static void Click(WebElement button, String str) {
       File file = new File(str);
       button.sendKeys(file.getAbsolutePath());
    }
     
    //рандом массива без повторяющихся чисел
    public Object [] Random_mass (int n, int min, int max){
        Object [] mass =  new Object [n];
        boolean flag = true;
        for(int i = 0; i <  n; i++){
            flag = true;
            Object value = Random_value (min, max);
            if (i == 0){
                mass[i] = value;
            }
            if (i >= 1){
                for (int j = 0; j < mass.length; j++){
                    if (mass[j] != null)
                    { 
                        if (mass[j] == value){
                            i--;
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag == true){
                    mass[i] = value; 
                }
            }
        }
        return mass;
    }
   
    public int Random_value (int min, int max){
        int value = (int)(min + Math.random() * max);
        return value;
    } 
    
    public void CompareTableXml (Map<String, String> table, Map<String, String> xml){
        boolean flag = false;
        for (String key1 : xml.keySet()) {    
            flag = false;
            for (String key2 : table.keySet()) {
                if (xml.get(key1).equals(table.get(key2))){
                    //значит такой фигурнат добавлен
                    flag = true;
            }
         }
            if (flag == false){
                //здесь сделать ассерт
                AssertCompareTableXml(xml.get(key1), flag);
            }
        } 
    } 

    
    
    public Map<String, String> ValueId (String path_file) throws IOException, JDOMException{
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(path_file);
        Document document = (Document) builder.build(xmlFile);
        Element rootNode = document.getRootElement();
        List list = rootNode.getChildren("entity");
           
        Map<String, String> hashmap = new HashMap<String, String>();

        for (int i = 0; i < list.size(); i++){      
            try{
              
                Element node = (Element) list.get(i);
                //проверка нода id
                Object id = node.getChildText("system_id");
                if (id != null){
                    hashmap.put(Integer.toString(i+1), id.toString()); 
                } else {
                    hashmap.put(Integer.toString(i+1), ""); 
                }
            }  catch (Exception ex){}
        }  
        return hashmap;
    } 
    
     //ожидание обработки
    public void WaitProcess(String locator, int count){
        WebDriverWait wait = new WebDriverWait(driver, count);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator))); 
    }
    
       //ожидание обработки
    public void WaitProcessInvisibility(String locator, int count){
            WebDriverWait wait = new WebDriverWait(driver, count);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator))); 
    }
    //ожидание кнопки закрыть "Обработка списка"
    public void WaitButton(By locator,int count){
        WebElement dynamicElement = (new WebDriverWait(driver, count)).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
    
    public void clearTable() throws Exception {
        sqlStatement(sql_query.CLEAR_TABLES);
    }
    
    public void doubleClick(By locator) {
       WebElement element = driver.findElement(locator);
        Actions actions = new Actions(driver);
        actions.doubleClick(element).perform();
    }

    //преобразование строки в массив
    public static String[] Divide(String s) {
        ArrayList<String> tmp = new ArrayList<String>();
        int i = 0;
        boolean f = false;

        for (int j = 0; j < s.length(); j++) {
            if (s.charAt(j) == '\n') {
                if (j > i) {
                    tmp.add(s.substring(i, j));
                }
                i = j + 1;
            }
        }
        if (i < s.length()) {
            tmp.add(s.substring(i));
        }
        return tmp.toArray(new String[tmp.size()]);
    }
    
    //сравнение таблицы PDLFIGURE и списка
    public void CompareTableSQL (String sql_query, String [] result) throws Exception{
        String pdl_SQL = Select(sql_query);
        String[] SQL = Divide(pdl_SQL);
       
        int k = 0;
        for (int i = 0; i < result.length; i++){
            k = 0;
            for (int j = 0; j < SQL.length; j++){
                if (result[i].equals(SQL[j])){
                    k++;
                }
            }
            if (k == 0){
                AssertElement(result[i], k);
            } else {
                AssertElement(result[i], k);
            } 
        }
    }
    
    public String Select (String SELECT) throws Exception {
        String result =  getStringFromDb(SELECT);
        return result;
    }

     //список таблицы
    public String [] ProcessTable(String object, int count, int column) {
        String [] result = new String [count];
        for (int i = 0; i < count; i++){
            result [i] = AddValue(object, i+1, column);
        }
        return result;
    }
    
    public String AddValue (String object, int i, int j){
        String figurant = object + i + "]/td[" + j + "]";
        String value  = driver.findElement(By.xpath(figurant)).getText();
        return value;
    }
    
    //возвращаем слово без пробела (\n)
    public String Str (String object){
        String value = "";
        for (int j = 0; j < object.length(); j++){
            if (object.charAt(j) != '\n') {
                value += object.charAt(j);
            }
        }  
        return value;
    }
    
    public void scrollDown(WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();" ,element);
    }
    
    public void scrollDown(String locator) {
        WebElement element = driver.findElement(By.xpath(locator));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();" ,element);
    }
    
    //сравнение результирующей таблицы с таблицый вердиктов 
    public void CompareTableSQL ( Map<String, String> hashmap) throws Exception {

      for (Map.Entry entry : hashmap.entrySet()) {
            String result = Select (sql_query.SELECT_COUNTID_PDLVERDICT + entry.getKey());
            String value =  Str(result);
            AssertCountVerdict((String) entry.getKey(),Integer.parseInt((String) entry.getValue()), Integer.parseInt(value));
        }
    }
}


//import java.io.IOException;
//import java.util.List;
//import org.jdom.Document;
//import org.jdom.Element;
//import org.jdom.JDOMException;
//import org.jdom.input.SAXBuilder;
//import org.junit.Assert;
//import ru.surgacheva.SmokeTestRPDL.pdl.figurant_pdl;


  //парсинг xml-файла. коллекция со списком фигурантов. 
    //фигурнаты, у которых некоторые ноды отсутствуют, записывается значение null
//    public List<figurant_pdl> ProcessXml(String path_file) throws JDOMException, IOException {
//        SAXBuilder builder = new SAXBuilder();
//        File xmlFile = new File(path_file);
//      
//            Document document = (Document) builder.build(xmlFile);
//            Element rootNode = document.getRootElement();
//            List list = rootNode.getChildren("entity");
//           
//           List<figurant_pdl> rows = new ArrayList<figurant_pdl>();
//            
//            int k = 0;
//            boolean flag = true;
//            for (int i = 0; i < list.size(); i++) {      
//                try{
//                    figurant_pdl figurantOb = new figurant_pdl();
//                    list.add(figurantOb);
//               //     List<String> row = new ArrayList<String>();
//                    flag = true;
//                    
//                    Element node = (Element) list.get(i);
//
//                    //проверка нода id
//                    Object id = node.getChildText("system_id");
//                    if (id != null){
//                        figurantOb.setIdInList(id.toString());
//                    } else {
//                        figurantOb.setIdInList("");
//                    }
//                    
//                    Element names = node.getChild("names");
//                    Element item = names.getChild("item");
//                    if (names != null){
//                         if (item != null){
//                            //проверка нода отчества
//                            Object middle_name = item.getChildText("middle_name"); 
//                            if (middle_name != null){
//                      //          row.add(k, middle_name.toString());
//                            } else { 
//                      //          row.add(k, "");
//                            } 
//                            
//                            //проверка нода фамилии  
//                            Object first_name = item.getChildText("first_name");
//                            if (first_name != null){
//                           //    row.add(k, first_name.toString());
//                            } else { 
//                            //    row.add(k, "");
//                            }
//
//                           //проверка нода имени  
//                            Object last_name = item.getChildText("last_name");
//                            if (last_name != null){
//                           //    row.add(k, last_name.toString());
//                            } else { 
//                            //    row.add(k, "");
//                            }                    
//                         } else {
//                             flag = false;
//                           //  row.add(k, "");
//                         }
//                    } else {
//                        flag = false;
//                       // row.add(k, "");
//                    }
//                    
//                    
//                    //проверка нода даты рождения
//                    Object dob = node.getChildText("dob"); 
//                    if (dob != null){
//               //         row.add(k, dob.toString());
//                    } else { 
//               //         row.add(k, "");
//                    }
//                    
//                     //проверка нода работы
//                    Element jobs = node.getChild("jobs");
//                    if(jobs != null)
//                    {
//                        Element items = jobs.getChild("item");
//                        if (items != null){
//                            Object jobsName = items.getChildText("name");
//                            if (jobsName != null){
//                       //         row.add(k, jobsName.toString());
//                            } else { 
//                       //         row.add(k, "");
//                            }
//                        } else {
//                    //      row.add(k, "");     
//                        }
//                    } else {
//                //          row.add(k, "");   
//                    }
//                    
//                    //проверка нода категории
//                    Element categories = node.getChild("categories");
//                    if (categories != null){
//                        Object type = categories.getChildText("item");
//                        if (type != null){
//                      //      row.add(k, type.toString());
//                        } else { 
//                     //       row.add(k, "");
//                        }
//                    } else {
//                   //     row.add(k, ""); 
//                    }
//                    
//                    
//                    
//                    if (flag == true){
//                    //    rows.add(row);
//                    }
//                    
//                    
//                    
//                }  catch (Exception ex){}
//            }
//            return rows;
//    } 




//     //список таблицы
//    public List<figurant_pdl> ProcessTable() {
//       
//        int count =  Count(By.xpath(count_row_table)); 
//        List <figurant_pdl> list = new ArrayList <figurant_pdl>();
//        
//        for (int i = 0; i < count; i++){
//            figurant_pdl figurantOb = new figurant_pdl();
//            list.add(figurantOb);
//                figurantOb.setIdInList(AddValue(i+1, 1));
//                figurantOb.setFirstName(AddValue(i+1, 2));
//                figurantOb.setLastName(AddValue(i+1, 3));
//                figurantOb.setSecondName(AddValue(i+1, 4));
//                figurantOb.setbDay(AddValue(i+1, 5));
//                figurantOb.setJob(AddValue(i+1, 6));
//                figurantOb.setType(AddValue(i+1, 7));
//            }
//        return list;
//    }
    
   
   
    
//      
//    public String AddValueXml (Element node, String strNode, String v){
//      //проверка нода id
//                    Object id = node.getChildText(strNode);
//                    if (id != null){
//                        figurantOb.setIdInList(id.toString());
//                    } else {
//                        figurantOb.setIdInList("");
//                    }
//    }
//    


   
//        String id = "";
//        String lastName = "";
//        String firstName = "";
//        String middle_name = "";
//        String dob = "";
//        String status = "";
//        String jobs = "";
//        String type = "";
//        
     
    

    
//    //ожидание
//    public void WebDriverWait(WebElement locator,int count){
//        WebElement dynamicElement = (new WebDriverWait(driver, count)).until(ExpectedConditions.presenceOfElementLocated((By) locator));
//    }
    


    
    
    
