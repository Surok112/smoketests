package ru.surgacheva.SmokeTestRPDL.pdl;

/**
 *
 * @author user
 */
public class figurant_pdl {
   
    private String idInList;
    private String lastName;
    private String firstName;
    private String secondName;
    private String bDay;
    private String job;
    private String type;

   
    public String getIdInList() {
        return idInList;
    }
    
    public void setIdInList(String idInList) {
        this.idInList = idInList;
    }
	
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
     
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getSecondName() {
        return secondName;
    }
  
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
   
    public String getbDay() {
        return bDay;
    }
   
    public void setbDay(String bDay) {
        this.bDay = bDay;
    }
   
    public String getJob() {
        return job;
    }
    
    public void setJob(String job) {
        this.job = job;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
