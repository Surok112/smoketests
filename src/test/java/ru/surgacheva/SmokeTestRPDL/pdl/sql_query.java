
package ru.surgacheva.SmokeTestRPDL.pdl;

public class sql_query {
    public static final String CLEAR_TABLES =
            "BEGIN ATOMIC\n" +
            "DELETE FROM RZ.H_PDLCOUNTRIES;\n" +
            "DELETE FROM RZ.H_PDLFIGURE;\n" +
            "DELETE FROM RZ.H_PDLNAMES;\n" +
            "DELETE FROM RZ.H_PDLRELATIVES;\n" +
            "DELETE FROM RZ.H_PDLVERDICT;\n" +
            "DELETE FROM RZ.PDLCLIENTUPDATE;\n" +
            "DELETE FROM RZ.PDLCOUNTRIES;\n" +
            "DELETE FROM RZ.PDLFIGURE;\n" +
            "DELETE FROM RZ.PDLNAMES;\n" +
            "DELETE FROM RZ.PDLRELATIVES;\n" +
            "DELETE FROM RZ.PDLSUSPECT;\n" +
            "DELETE FROM RZ.PDLVERDICT;\n" +
            "DELETE FROM RZ.PDLCACHE;\n" +
            "DELETE FROM RZ.PDLCATEGORY;\n" +
            "DELETE FROM RZ.PDLEVERACTIVATEDCLIENTS;\n" +
            "DELETE FROM RZ.PDLLIST;\n" +
            "DELETE FROM RZ.PDLREPORT;" +
            "END";
    
    public static final String SELECT_PDL_ID =
    "SELECT PDL_ID FROM RZ.PDLFIGURE";
    
    public static final String SELECT_FOR_SEARCH =
    "SELECT FOR_SEARCH FROM RZ.PDLFIGURE WHERE PDL_ID=";
    
    public static final String SELECT_ID_PDLCOUNTRIES =
    "SELECT PDL_ID FROM RZ.PDLCOUNTRIES";
    
    public static final String SELECT_ID_PDLVERDICT =
    "SELECT PDL_ID FROM RZ.PDLVERDICT WHERE PDL_ID=";
      
    public static final String SELECT_COUNTID_PDLVERDICT =
    "SELECT COUNT FROM RZ.PDLVERDICT WHERE PDL_ID=";
        
    public static final String SELECT_ISPDL_PDLVERDICT =
    "SELECT IS_PDL FROM RZ.PDLVERDICT WHERE CONT_ID=";
         
     public static final String SELECT_COMMENT_PDLVERDICT =
    "SELECT COMMENT FROM RZ.PDLVERDICT WHERE CONT_ID=";
     
     
}
