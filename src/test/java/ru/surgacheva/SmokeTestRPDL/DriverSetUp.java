package ru.surgacheva.SmokeTestRPDL;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.PageFactory;


public class DriverSetUp {

    public static final String MAIN_PAGE = "http://localhost:9080/markup/pages/rpdl_load_list.html";
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        FirefoxProfile profile = new FirefoxProfile();
        driver = new FirefoxDriver(profile);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        RDPL_methods page = PageFactory.initElements(driver, RDPL_methods.class);
        driver.get(MAIN_PAGE);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    public String getMainPage() {
        return MAIN_PAGE;
    }
}
