package ru.surgacheva.SmokeTestRPDL;

import org.junit.Test;
import org.openqa.selenium.By;
import ru.surgacheva.SmokeTestRPDL.Pages.RPDL_page;

public class PositivTest extends TestCases{
  
    //Позитивный кейс - Активности кнопки 
    @Test
    public void case01() throws Exception {
        TestCase01();
    }
    
    //Позитивный кейс - Загрузка xml-файла
    @Test
    public void case02() throws Exception {
        TestCase02();
    }

     //Позитивный кейс - Сохранение списка фигурантов в бд
    @Test
    public void case03() throws Exception  {
        TestCase03();
    }
    
    @Test
    public void case04() throws Exception  {
       TestCase04();
    }
    
    //Позитивный кейс - Сохранение фигурантов в бд, которые не отправлены на поиск совпадений.
    @Test
    public void case05() throws Exception  {
        TestCase05();
    }
    
     //Позитивный кейс -  Отображение списка фигурантов на странице изменения решения
    @Test
    public void case06() throws Exception  {
        TestCase06();
    }
    
     //Позитивный кейс -  Отображение списка фигурантов на странице изменения решения
    @Test
    public void case07() throws Exception  {
         TestCase07();
    }
    
    //Позитивный кейс -  Добавление комментария у фигуранта
    @Test
    public void case08() throws Exception  {
         TestCase08();
    }

     //Позитивный кейс -  Отображение комментария у фигуранта
    @Test
    public void case09() throws Exception  {
         TestCase09();
    }

    
   
    
    
    
    
    
        
        
     
//    public void AssertIsAlertPresent()
//        {
//            Trace.WriteLine("Assert is the alert present:", "Document");
//            Assert.IsTrue(ReturnAlert() != null, "Error! The alert is not present");
//        }
        
        

        //Обработка списка
        
        //Выбор фигурантов, которые не пойдут на обработку
      //  Process();
        //установка правил
      //  SettingRules();
        
        //обработка совпадений
       // ProcessingMatch();
         //измененеие выбора по совпадениям
       // changeSelection();
    }



   
         //   WebDriverWait(button_load,10);
        //ожидание появления первого результата в выдаче
    //    WebDriverWait(By.xpath(".//*[@id='rso']/div/div[1]/div/h3/a"),10);
        //подсчет ссылок в поисковой выдаче       
     //   int count= Count(By.xpath("//div[@class='g']"));
        //проверка
    //    Assert.assertTrue("Количество записей на странице "+count,Check(count,10));