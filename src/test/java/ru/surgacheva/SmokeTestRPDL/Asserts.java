package ru.surgacheva.SmokeTestRPDL;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Asserts extends ConnectionDb {
    
    private static final Logger log = Logger.getLogger(Asserts.class);
     
    public boolean ActivityButton(WebElement button){
        boolean flag = button.isEnabled(); 
        if (flag == true){
             Assert.assertTrue("Кнопка выбора файла активна!", flag);
            log.info("Кнопка выбора файла активна");
        } else {
            log.info("Кнопка выбора файла не активна");
            Assert.assertTrue("Кнопка выбора файла не активна!", flag);
        }
        return flag;
    }
    
    public boolean elementExist(By locator) {
        return driver.findElements(locator).size() > 0;
    }
    
    public void OpenPdlProcessingPage(String str, String path_file) {
        if (elementExist(By.xpath(str))) {
            Assert.assertTrue("Файл " + path_file + " загружен! Страница открыта!", elementExist(By.xpath(str)));
            log.info("Открыта страница обработки списка " + path_file);
        } 
        else {
            RDPL_methods mtds = new RDPL_methods();
            mtds.Click(button_Close);
            log.info("Файл " + path_file + " не загружен! Страница не открыта!");
            Assert.assertTrue("Файл " + path_file + " не загружен! Страница не открыта!", elementExist(By.xpath(str)));
        }
    }
     
    public void NotOpenPdlProcessingPage(String str, String path_file) {
        if (!elementExist(By.xpath(str))) {
            RDPL_methods mtds = new RDPL_methods();
            mtds.Click(button_Close);
            log.info("Файл " + path_file + " не загружен! Страница не открыта!");
            Assert.assertFalse("Файл " + path_file + " не загружен! Страница не открыта!", elementExist(By.xpath(str)));
        } 
         else {
             Assert.assertFalse("Файл " + path_file + " загружен! Страница открыта!", elementExist(By.xpath(str)));
             log.info("Открыта страница обработки списка " + path_file);
        }
    }
       
       
    public void AssertElement(String Element, int k){
        if (k != 0){
            Assert.assertTrue("Фигурант " + Element + " загружен в таблицу PDLFIGURE!", true);
            log.info("Фигурант " + Element + " загружен в таблицу PDLFIGURE!");
        } else {
            log.info("Фигурант " + Element + " не загружен в таблицу PDLFIGURE!");
            Assert.assertTrue("Фигурант " + Element + " не загружен в таблицу PDLFIGURE!", false);
        }
    }
    
     public void AssertCheckFOR_SEARCH(String Element, int k){
        if (k == 0){
         //  таблице FOR_SEARCH.PDLFIGURE = 0.
            Assert.assertTrue("У фигуранта " + Element + " FOR_SEARCH.PDLFIGURE = 0!", true);
            log.info("У фигуранта " + Element + " FOR_SEARCH.PDLFIGURE = 0!");
        } else {
            log.info("У фигуранта " + Element + " FOR_SEARCH.PDLFIGURE = 1!");
            Assert.assertTrue("У фигуранта " + Element + " FOR_SEARCH.PDLFIGURE = 1!", false);
        }
    }
     
    public void AssertCountVerdict(String Element, int value1, int value2 ){
        if (value1 == value2){
            Assert.assertTrue("Фигурант " + Element + " отображается в таблице PDLVERDICT!", true);
            log.info("Фигурант " + Element + " отображается в таблице PDLVERDICT!");
        } else {
            log.info("Фигурант " + Element + " не отображается в таблице PDLVERDICT!");
            Assert.assertTrue("Фигурант " + Element + " не отображается в таблице PDLVERDICT!", false);
        } 
    }  
    
    public void AssertChangeVerdict(String Element, int value1, int value2){
        if (value1 != value2){
            Assert.assertTrue("У фигурната " + Element + " изменился признак IS_PDL!", true);
            log.info("У фигурната " + Element + " изменился признак IS_PDL!");
        } else {
            log.info("У фигурната " + Element + " не изменился признак IS_PDL!");
            Assert.assertTrue("У фигурната " + Element + " не изменился признак IS_PDL!", false);
        } 
    }
    
    public void AssertAddComment(String Element, String addValue, String result){
        if (addValue.equals(result)){
            Assert.assertTrue("Комментарий добавлен! У фигурната " + Element + " COMMENT.PDLVERDICT принимает значение введенной строки!", true);
            log.info("Комментарий добавлен! У фигурната " + Element + " COMMENT.PDLVERDICT принимает значение введенной строки!");
        } else {
            log.info("У фигурната " + Element + " COMMENT.PDLVERDICT не принимает значение введенной строки!");
            Assert.assertTrue("У фигурната " + Element + " COMMENT.PDLVERDICT не принимает значение введенной строки!", false);
        } 
    }
    
    public void AssertViewComment(String Element, String comment, String commentTable){
        if (comment.equals(commentTable)){
            Assert.assertTrue("Комментарий у " + Element + " отображается!", true);
            log.info("Комментарий у " + Element + " отображается!");
        } else {
            log.info("Комментарий у " + Element + " не отображается!");
            Assert.assertTrue("Комментарий у " + Element + " не отображается!", false);
        } 
    }
    
    public void AssertCompareTableXml (String Element, boolean flag){
        if (!flag){
            Assert.assertTrue("Фигурант с id= "+ Element +" не добавлен в таблицу", true);
            log.info("Фигурант с id= "+ Element +" не добавлен в таблицу");   
        } else {
            log.info("Фигурант с id= "+ Element +" добавлен в таблицу");
            Assert.assertTrue("Фигурант с id= "+ Element +" добавлен в таблицу", false);
        }
    }
    
}
     
//    public boolean isAlertPresent() {
//        boolean presentFlag = false;
//        try {
//            Alert alert = driver.switchTo().alert();
//            presentFlag = true;
//          //  alert.accept();
//        } catch (NoAlertPresentException ex) {
//            ex.printStackTrace();
//        }
//        return presentFlag;
//    }

    
//    public Alert ReturnAlert() {
//            //alert - обычное окно сообщения, у которого есть только одна кнопка, которая собственно это окно и закрывает
//            Alert  alert =  driver.switchTo().alert();
//            return alert;
//        }

  

