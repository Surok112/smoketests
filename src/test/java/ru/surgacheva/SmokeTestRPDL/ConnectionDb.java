package ru.surgacheva.SmokeTestRPDL;

import java.sql.*;

public class ConnectionDb extends RDPL_variables {
    
    String jdbcClassName = "com.ibm.db2.jcc.DB2Driver";
    String url = "jdbc:db2://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "";
    String user = DB_USER_NAME;
    String password = DB_USER_PASSWORD;
    Connection connection = null;
    Statement stmt = null;
    ResultSet rs = null;
    
    //подключение к DB2
    private Connection getConnection() throws Exception {
        try {
            Class.forName(jdbcClassName);
            connection = DriverManager.getConnection(url, user, password);
        } catch(ClassNotFoundException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }

    //закрытие соединения
    private void closeConnection(Connection connection) {
        try {
            if (connection == null) { return; }
            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                assert connection != null;
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
     //очищение таблицы
    public void sqlStatement(String statement) throws Exception {
        Connection connection = getConnection();
        connection.setAutoCommit(false);
        stmt = connection.createStatement();
        stmt.executeUpdate(statement);
        stmt.close();
        connection.commit();
        closeConnection(connection);
    }
    
     //выполняет выгрузку из БД
    public String getStringFromDb(String statement) throws Exception {
        String resultString = "";
        Connection connection = getConnection();
        stmt = connection.createStatement();
        rs = stmt.executeQuery(statement);
        while (rs.next()) {
            resultString += rs.getString(1) +  "\n";
        }
//        System.out.println("Fill results into string: \n" + resultString);
        rs.close();
        stmt.close();
        connection.commit();
        closeConnection(connection);
        return resultString;
    }
}
