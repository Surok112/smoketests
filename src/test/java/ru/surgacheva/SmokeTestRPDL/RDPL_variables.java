package ru.surgacheva.SmokeTestRPDL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static ru.surgacheva.SmokeTestRPDL.DriverSetUp.driver;

/**
 *
 * @author user
 */
public class RDPL_variables extends DriverSetUp{
    
    public String loadPage = "http://localhost:9080/markup/pages/rpdl_proc_list.html";
    
    public static String path_file_pos = "C:/SurgachevaOksana/src/test/java/ru/surgacheva/SmokeTestRPDL/files/RPDL_List.xml";
    public static String path_file_neg = "C:/SurgachevaOksana/src/test/java/ru/surgacheva/SmokeTestRPDL/files/RPDL.zip";
    public static String path_fileNotIdNodes = "C:/SurgachevaOksana/src/test/java/ru/surgacheva/SmokeTestRPDL/files/RPDLnotMainNodes.xml";
    public static String path_fileNotConcomitantNodes = "C:/SurgachevaOksana/src/test/java/ru/surgacheva/SmokeTestRPDL/files/RPDLnotConcomitantNodes.xml";
      
    @FindBy(how = How.XPATH, using = ".//*[@id='file']") public WebElement button_select;
    public String buttonSelect= ".//*[@id='file']";
    @FindBy(how = How.XPATH, using = " .//*[@id='loadPdlFile']/div[2]/form/div/div/div") public WebElement button_Select;
    @FindBy(how = How.XPATH, using = ".//div[4]/div[1]/div/a/span") public WebElement button_Close;
    @FindBy(how = How.XPATH, using = ".//*[@id='closeWindowButton']") public WebElement SaveClose;
    public String Save_Close = ".//*[@id='closeWindowButton']";
    public String editPdlClients = ".//*[@id='editPdlClientsGridR']";
    public String editIdCheck = ".//*[@id='editPdlClientsGridR']/div[@class='k-grid-content']/table/tbody/tr[1]//span[contains(@class, 'client')]";
    @FindBy(how = How.XPATH, using = ".//*[@id='editPdlClientsGridR']/div[@class='k-grid-content']/table/tbody/tr[11]//span[contains(@class, 'commentEdit k-icon k-i-pencil pointer')]") public WebElement editComment;
    public String textComment = ".//*[@id='editPdlClientsGridR']/div[@class='k-grid-content']/table/tbody/tr[11]//span[contains(@class, 'comment')]";
    public String windowComment = "//body/div[5]";
    public String windowVerdict = "//body/div[4]"; 
    public String editCheck = ".//*[@id='editPdlClientsGridR']/div[@class='k-grid-content']/table/tbody/tr[12]//ins[@class='iCheck-helper']";
    @FindBy(how = How.XPATH, using = "//*[@id='listId']") public WebElement indentification;
    @FindBy(how = How.XPATH, using = ".//*[@id='commentText']") public WebElement comment;
    @FindBy(how = How.XPATH, using = "//*[@id='saveListIdKendo']") public WebElement button_OK;
    public String buttonOK= "//*[@id='saveListIdKendo']";
    @FindBy(how = How.XPATH, using = ".//*[@id='loadPdlFile']/div[2]/form/div/button") public WebElement button_load;     
    public String count_row_table=".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr";
    @FindBy(how = How.XPATH, using = ".//*[@id='saveCommentKendo']") public WebElement saveComment;     
    public String count_row_tableEdit = ".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr";
    public String tableProcess = ".//*[@id='procPdlListGrid']/div[3]/table";
    @FindBy(how = How.XPATH, using = ".//*[@id='saveList']") public WebElement button_save;
    public String dialogButYes=".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr";
    @FindBy(how = How.XPATH, using = ".//*[@id='dialogButtonYes']") public WebElement dialogButtonYes;
    @FindBy(how = How.XPATH, using = ".//*[@id='nameList']") public WebElement nameList;
    @FindBy(how = How.XPATH, using = ".//div[5]/div[1]/div/a/span") public WebElement button_close;
    @FindBy(how = How.XPATH, using = ".//div[4]/div[1]/div/a[1]/span") public WebElement button_window;
    public String oneRules = ".//*[@id='acceptGrid']/table/tbody/tr[1]/td[1]/div/ins";
    @FindBy(how = How.XPATH, using = ".//*[@id='settingsSubmit']") public WebElement buttonSaveRules;
    public String buttonClose = ".//div[5]/div[1]/div/a/span";
    @FindBy(how = How.XPATH, using = ".//div[3]/div[1]/div/a/span") public WebElement button_close2;
    public String buttonClose2 = ".//div[3]/div[1]/div/a/span";
    @FindBy(how = How.XPATH, using = ".//div[6]/div[1]/div/a/span") public WebElement button_close3;
    public String buttonClose3 = ".//div[6]/div[1]/div/a/span";
    @FindBy(how = How.XPATH, using = ".//*[@id='rpdl_edit_verdicts']") public WebElement changePage;
    public String change_Page = ".//*[@id='rpdl_edit_verdicts']";
    @FindBy(how = How.XPATH, using = ".//*[@id='rpdl_edit_verdicts']") public WebElement changeVerdict;
    public String value =  ".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[1]/td[1]";
  
    //для бд
    public final String DB_HOST = "10.0.61.5";
    public final String DB_PORT = "50000";
    public final String DB_NAME = "MDMDB1";
    public final String DB_USER_NAME = "mdm";
    public final String DB_USER_PASSWORD = "secret";
    
    public RDPL_variables() {
        PageFactory.initElements(driver, this);
    }
    
    
    
    
      
    
}
