package ru.surgacheva.SmokeTestRPDL;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import static ru.surgacheva.SmokeTestRPDL.DriverSetUp.driver;
import ru.surgacheva.SmokeTestRPDL.Pages.RPDL_page;
import ru.surgacheva.SmokeTestRPDL.pdl.sql_query;

public class TestCases extends RPDL_page{
  
    public void TestCase01() throws Exception {
        ActivityButton(button_Select);
    }
    
    public void TestCase02() throws Exception {
        clearTable();
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        OpenPdlProcessingPage(tableProcess, path_file_pos);
        SaveFile();
        SettingRules();
        ProcessingMatch();
        changeSelection();
    }

    public void TestCase03() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        int count =  Count(By.xpath(count_row_table)); 
        String [] result = ProcessTable(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[",count, 1);
        SaveFile();
        CompareTableSQL(sql_query.SELECT_PDL_ID, result);
        SettingRules();
        ProcessingMatch();
        changeSelection();
    }
    
      public void TestCase04() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        Thread.sleep(1000);
        LoadFile(path_file_pos);
        
        int count =  Count(By.xpath(count_row_table)); 
        String [] result = ProcessTable(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[" , count, 1);
        SaveFile();
        CompareTableSQL(sql_query.SELECT_ID_PDLCOUNTRIES, result);
        //завершение обработки списка
        SettingRules();  
    }
      
    public void TestCase05() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        
        int count =  Count(By.xpath(count_row_table)); 
        int n = DoNotSend();
        //перед сохранением узнать номер ид этих фигурантов и загнать в массив
        String [] checkNotSend = CheckDoNotSend(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[", n, count);
        SaveFile();
        //запрос к бд к кашдому ид
        CheckFOR_SEARCH(checkNotSend, sql_query.SELECT_FOR_SEARCH);
        SettingRules();   
        ProcessingMatch();
        changeSelection();
    }
    
    public void TestCase06() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        SaveFile();
        SettingRules();   
        ProcessingMatch();
        changeSelection();
        int count =  Count(By.xpath(count_row_tableEdit)); 
        //получаем Map фигурнатов в результирующей таблице
         Map<String, String> hashmap = new HashMap<String, String>();
         hashmap = KeyValueProcessTable(".//*[@id='editPdlVerdictsGrid']/div[2]/table/tbody/tr[", count, 1, 7);
         CompareTableSQL(hashmap);
    }
     
    public void TestCase07() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        SaveFile();
        SettingRules();   
        ProcessingMatch();
        changeSelection();
        int count =  Count(By.xpath(count_row_tableEdit));   
        changeVerdict(count);
    }
      
    public void TestCase08() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        SaveFile();
        SettingRules();   
        ProcessingMatch();
        changeSelection();
        int count =  Count(By.xpath(count_row_tableEdit));   
        AddComment_(count); 
    }
    
    public void TestCase09() throws Exception  {
        clearTable();
        driver.get(MAIN_PAGE);
        WaitProcess(buttonSelect, 1000);
        LoadFile(path_file_pos);
        SaveFile();
        SettingRules();   
        ProcessingMatch();
        changeSelection();
        int count =  Count(By.xpath(count_row_tableEdit));   
        ViewComment(count);
    }
    
    public void TestCase10() throws Exception  {
        clearTable();
        LoadFile(path_file_neg);
        NotOpenPdlProcessingPage(tableProcess, path_file_neg);       
    }
    
    public void TestCase11() throws Exception  {
        clearTable();
        LoadFile(path_fileNotIdNodes);
        int count = Count(By.xpath(count_row_table)); 
        Map<String, String> table = KeyValueProcessTable(".//*[@id='procPdlListGrid']/div[3]/table/tbody/tr[", count, 1);
        Map<String, String> xml = ValueId(path_fileNotIdNodes);
        CompareTableXml(table, xml );
        
    }

    
    
}
